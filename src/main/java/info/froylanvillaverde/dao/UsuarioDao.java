package info.froylanvillaverde.dao;

import info.froylanvillaverde.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Froy
 */
public interface UsuarioDao extends JpaRepository<Usuario, Long>{
    
    Usuario findByUsername(String username);
}
