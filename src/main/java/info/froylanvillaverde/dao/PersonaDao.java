package info.froylanvillaverde.dao;

import info.froylanvillaverde.model.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Froy
 */
public interface PersonaDao extends JpaRepository<Persona, Long>{//Extiende de CrudRepository en lugar de anotarla con @Repository, no es necesario implementar los metodos de listar, actualizar, crear y eliminar
    
}
