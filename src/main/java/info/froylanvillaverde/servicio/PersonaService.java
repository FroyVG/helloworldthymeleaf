package info.froylanvillaverde.servicio;

import info.froylanvillaverde.model.Persona;
import java.util.List;

/**
 *
 * @author Froy
 */
public interface PersonaService {
    
    public List<Persona> listarPersonas();
    
    public void guardar(Persona persona);
    
    public void eliminar(Persona person);
    
    public Persona encontrarPersona(Persona persona);
}
