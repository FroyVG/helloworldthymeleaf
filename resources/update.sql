/**
 * Author:  Froy
 * Created: 27/03/2020
 */

create schema test;

create table persona
(
	id int auto_increment,
	nombre varchar(50) null,
	apellido varchar(50) null,
	email varchar(50) null,
	telefono varchar(50) null,
	constraint persona_pk
		primary key (id)
);

create table usuario
(
	id int auto_increment,
	username varchar(50) null,
	password varchar(128) null,
	constraint usuario_pk
		primary key (id)
)
comment 'Tabla que contiene los usuarios';

create table rol
(
	id int auto_increment,
	nombre varchar(50) null,
	id_usuario int null,
	constraint rol_pk
		primary key (id),
	constraint id_rol_usuario
		foreign key (id_usuario) references usuario (id)
)
comment 'Tabla que contiene los roles';

--Insertar datos en las tablas
INSERT INTO test.rol (nombre, id_usuario) VALUES ('ROLE_ADMIN', 1);
INSERT INTO test.rol (nombre, id_usuario) VALUES ('ROLE_USER', 1);
INSERT INTO test.rol (nombre, id_usuario) VALUES ('ROLE_USER', 2);

INSERT INTO test.usuario (username, password) VALUES ('admin', '$2a$10$gWpIfXWOvAFJ5tpVEzvhOOJg8kVQVxiHFU.ZMZT4u6Ty5l4SfeWxu');
INSERT INTO test.usuario (username, password) VALUES ('user', '$2a$10$w.nytDUTdPJ.FfehOkTqEebx1Qfe7Y.dAZCrvA8Z0vDdxVq/AvbUa');

--Se agrega nnueva columna en versión 6
alter table persona add saldo double null;